/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/16 11:51:11 by davifah           #+#    #+#             */
/*   Updated: 2022/05/16 12:09:05 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	create_pipe(int *fd)
{
	if (pipe((int *)fd))
	{
		perror("Pipe: ");
		return (1);
	}
	return (0);
}

void	print_fd_read(int fd)
{
	int		n;
	char	*buff[16];

	n = 15;
	while (n >= 15)
	{
		ft_bzero(buff, 16);
		n = read(fd, buff, 15);
		ft_printf("%s", buff);
	}
}
