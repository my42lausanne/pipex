/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/25 09:49:06 by dfarhi            #+#    #+#             */
/*   Updated: 2022/05/16 12:42:22 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	main(int ac, char **av, char **envp)
{
	int		err;

	err = 0;
	if (ac >= 5)
		err += pipex(ac, av, envp);
	else
		err += error("Not enough arguments\n");
	if (err)
		return (1);
	else
		return (0);
}

int	error(char *str)
{
	ft_putstr_fd(str, 2);
	return (1);
}
