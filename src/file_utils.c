/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: davifah <dfarhi@student.42lausanne.ch      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/11 15:15:46 by davifah           #+#    #+#             */
/*   Updated: 2022/05/16 13:28:13 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

int	open_infile(char *infile)
{
	int	fdin;

	if (ft_strncmp(infile, "here_doc", 9))
	{
		fdin = open(infile, O_RDONLY);
		if (fdin < 0)
		{
			perror(infile);
			return (-1);
		}
		return (fdin);
	}
	return (-2);
}

int	open_outfile(char *heredoc, char *outfile)
{
	int	flags;
	int	fdout;

	flags = O_WRONLY | O_CREAT;
	if (!ft_strncmp(heredoc, "here_doc", 9))
		flags = flags | O_APPEND;
	else
		flags = flags | O_TRUNC;
	fdout = open(outfile, flags, 0644);
	if (fdout < 0)
	{
		perror(outfile);
		return (-1);
	}
	return (fdout);
}
