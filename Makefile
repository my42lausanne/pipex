# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2022/05/16 14:06:26 by davifah          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FILES		= main pipex check_file_permissions env_path file_utils heredoc	\
			  pipex_utils exec_command

FILES		:= $(addprefix src/, ${FILES})
FILES		:= $(addsuffix .c, ${FILES})
OBJS		= ${FILES:.c=.o}

NAME		= pipex

CC			= gcc -Wall -Wextra -Werror

INCLUDES	= -I./includes -I./libft/includes

LIB			= -L./libft/ -lft
LIBFT		= libft/libft.a
LIBFT_ARGS	=

SYSTEM		= $(shell uname -s)

${NAME}:	${LIBFT} ${OBJS}
			${CC} ${INCLUDES} -o ${NAME} ${OBJS} ${LIB}

.c.o:
			${CC} -c ${INCLUDES} $< -o ${<:.c=.o}

all:		${NAME}

AddressSanitizer:	CC := ${CC} -fsanitize=address -g
ifeq ($(SYSTEM), Linux)
AddressSanitizer:	CC := ${CC} -static-libasan
endif
AddressSanitizer:	LIBFT_ARGS := ${LIBFT_ARGS} ADDRESS_SANITIZER=1
AddressSanitizer:	${NAME}

# cmd to prof code:
# gprof ${NAME} gmon.out > analysis.txt
profile:	fclean
profile:	CC := ${CC} -pg
profile:	LIBFT_ARGS := ${LIBFT_ARGS} PROFILE=1
profile:	${NAME}

${LIBFT}:
			$(MAKE) -C ./libft ft_printf get-next-line ${LIBFT_ARGS}

git:
			git submodule update --init --recursive

clean:
			rm -f ${OBJS}
			make -C ./libft clean

fclean:		clean
			rm -f ${NAME} libft/libft.a

re:			fclean all

.PHONY:		all clean fclean re
