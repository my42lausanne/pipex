/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/25 09:46:09 by dfarhi            #+#    #+#             */
/*   Updated: 2022/05/16 13:59:32 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

// TEST LIBRARIES
# include <sys/stat.h>
// END
# include "ft_printf.h"
# include "expanded.h"
# include "get_next_line.h"

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <string.h>
# include <errno.h>
# include <sys/types.h>
# include <sys/wait.h>

typedef struct s_env
{
	char	**envp;
	char	**env_path;
}	t_env;

int		pipe_test(char *cmd, t_env env, char *infile);
t_env	parse_env_path(char **envp);
void	free_word_list(char **env_path);
int		open_infile(char *infile);
int		open_outfile(char *heredoc, char *outfile);
int		pipex(int ac, char **av, char **envp);
int		heredoc(char *delimiter, int fdout);
int		pipex_heredoc(char *delimiter);
int		exec_command(char *cmd, char **envp, int fdout, int fdin);

// MAIN FILE
int		error(char *str);
int		check_file_permissions(int ac, char **av);

// UTILS
int		create_pipe(int *fd);

// TEST
void	print_fd_read(int fd);

#endif
